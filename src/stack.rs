use std::fmt::{Display, Formatter, Result as FmtResult};

use crate::value::Value;


pub type StackPtr = usize;

pub struct Stack {
    data: Vec<Value>,
}

impl Stack {
    pub fn new(size: StackPtr) -> Self {
        let data = Vec::with_capacity(size);
        Self { data }
    }

    pub fn ptr(&self) -> StackPtr {
        self.data.len() as StackPtr
    }

    pub fn push(&mut self, value: Value) -> Option<StackPtr> {
        if self.data.len() < self.data.capacity() {
            let ptr = self.data.len() as StackPtr;
            self.data.push(value);
            Some(ptr)
        } else {
            None
        }
    }

    pub fn pop(&mut self) -> Option<Value> {
        self.data.pop()
    }

    pub fn get_ref(&self, ptr: StackPtr) -> Option<&Value> {
        self.data.get(ptr)
    }

    pub fn get_mut(&mut self, ptr: StackPtr) -> Option<&mut Value> {
        self.data.get_mut(ptr)
    }

    pub fn dig_ref(&self, offset: StackPtr) -> Option<&Value> {
        if offset < self.data.len() {
            let ptr = self.data.len() - 1 - offset;
            self.get_ref(ptr)
        } else {
            None
        }
    }

    pub fn dig_mut(&mut self, offset: StackPtr) -> Option<&mut Value> {
        if offset < self.data.len() {
            let ptr = self.data.len() - 1 - offset;
            self.get_mut(ptr)
        } else {
            None
        }
    }

    pub fn swap(&mut self, ptr_a: StackPtr, ptr_b: StackPtr) -> bool {
        if ptr_a < self.ptr() && ptr_b < self.ptr() {
            self.data.swap(ptr_a, ptr_b);
            true
        } else {
            false
        }
    }

    pub fn top(&mut self, ptr: StackPtr) -> bool {
        match self.data.get(ptr) {
            Some(value) => {
                let cloned = value.clone();
                match self.push(cloned) {
                    Some(_) => true,
                    None => false,
                }
            }
            None => false,
        }
    }
}

impl Display for Stack {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        Display::fmt(&"[ ", f)?;
        let mut iter = self.data.iter();
        if let Some(value) = iter.next() {
            Display::fmt(&value, f)?;
        }
        for value in iter {
            Display::fmt(&" | ", f)?;
            Display::fmt(&value, f)?;
        }
        Display::fmt(&" ]", f)
    }
}
