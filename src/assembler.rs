use std::mem;
use std::collections::HashMap;
use std::fmt::{Display, Formatter, Result as FmtResult};


#[derive(Debug)]
pub enum Error {
    UnexpectedToken(SourceLocation, String),
    UnexpectedEndOfStream,
    InvalidCharacter(SourceLocation, char),
    InvalidInteger(SourceLocation, String),
    InvalidOpcode(SourceLocation, String),
    InvalidDirective(SourceLocation, String),
    UnknownLabel(SourceLocation, String),
    UnclosedDelimiter(SourceLocation),
    MissingEntryPoint,
    DuplicateLabel(SourceLocation, String),
}

impl Error {
    pub fn location(&self) -> Option<SourceLocation> {
        match self {
            Error::UnexpectedToken(location, ..)
            | Error::InvalidCharacter(location, ..)
            | Error::InvalidInteger(location, ..)
            | Error::InvalidOpcode(location, ..)
            | Error::InvalidDirective(location, ..)
            | Error::UnknownLabel(location, ..)
            | Error::UnclosedDelimiter(location, ..)
            | Error::DuplicateLabel(location, ..) => Some(*location),
            _ => None
        }
    }
}

impl Display for Error {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Error::UnexpectedToken(location, name) =>
                f.write_fmt(format_args!("{}: Unexpected token '{}'", location, name)),
            Error::UnexpectedEndOfStream =>
                f.write_fmt(format_args!("Unexpected end of source!")),
            Error::InvalidCharacter(location, character) =>
                f.write_fmt(format_args!("{}: Invalid character '{}'!", location, character)),
            Error::InvalidInteger(location, integer) =>
                f.write_fmt(format_args!("{}: Invalid integer '{}'!", location, integer)),
            Error::InvalidOpcode(location, opcode) =>
                f.write_fmt(format_args!("{}: Invalid opcode '{}'!", location, opcode)),
            Error::InvalidDirective(location, directive) =>
                f.write_fmt(format_args!("{}: Invalid directive '{}'!", location, directive)),
            Error::UnknownLabel(location, label) =>
                f.write_fmt(format_args!("{}: Unknown label '{}'!", location, label)),
            Error::UnclosedDelimiter(location) =>
                f.write_fmt(format_args!("{}: Unclosed delimiter!", location)),
            Error::MissingEntryPoint =>
                f.write_fmt(format_args!("Entry point not defined (Missing directive .entry)!")),
            Error::DuplicateLabel(location, label) =>
                f.write_fmt(format_args!("{}: Duplicate label '{}'!", location, label)),
        }
    }
}


#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub struct SourceLocation {
    pub line: usize,
    pub index: usize,
}

impl Display for SourceLocation {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        f.write_fmt(format_args!("[{}:{}]", self.line + 1, self.index + 1))
    }
}

impl std::cmp::PartialOrd<Self> for SourceLocation {
    fn partial_cmp(&self, other: &Self) -> Option<std::cmp::Ordering> {
        if self.line > other.line {
            Some(std::cmp::Ordering::Greater)
        } else if self.line < other.line {
            Some(std::cmp::Ordering::Less)
        } else if self.index > other.index {
            Some(std::cmp::Ordering::Greater)
        } else if self.index < other.index {
            Some(std::cmp::Ordering::Less)
        } else {
            Some(std::cmp::Ordering::Equal)
        }
    }
}


#[derive(Copy, Clone, Debug)]
enum TokenData<'a> {
    Identifier(&'a str),
    Number(&'a str),
    Colon,
    Semicolon,
    Comma,
    Period,
    Quoted(&'a str),
}

impl<'a> Display for TokenData<'a> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            TokenData::Identifier(token) =>
                f.write_fmt(format_args!("identifier '{}'", token)),
            TokenData::Number(token) =>
                f.write_fmt(format_args!("number literal '{}'", token)),
            TokenData::Colon =>
                f.write_str("colon (:)"),
            TokenData::Semicolon =>
                f.write_str("semicolon (;)"),
            TokenData::Comma =>
                f.write_str("comma (,)"),
            TokenData::Period =>
                f.write_str("period (.)"),
            TokenData::Quoted(string) =>
                f.write_fmt(format_args!("string '{}'", string)),
        }
    }
}


struct Token<'a> {
    location: SourceLocation,
    data: TokenData<'a>,
}

impl<'a> Display for Token<'a> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        let line = self.location.line;
        let index = self.location.index;
        f.write_fmt(format_args!("[{}:{}] {}", line, index, self.data))
    }
}


struct TokenList<'a> {
    tokens: Vec<Token<'a>>
}

impl<'a> TokenList<'a> {
    pub fn tokenize(assembly: &'a str) -> Result<Self, Error> {
        enum Mode {
            None,
            Comm,
            Name,
            Num,
            Quot,
        }

        let mut tokens = Vec::new();
        let mut mode = Mode::None;
        let mut line_idx = 0;
        let mut line_start_idx = 0;
        let mut token_start_idx = 0;
        let mut token_location = SourceLocation { line: 0, index: 0 };
        let chars = assembly.chars()
            .chain("".chars())
            .enumerate();
        'outer: for (total_idx, curr) in chars {
            loop {
                let location = SourceLocation {
                    line: line_idx,
                    index: total_idx - line_start_idx,
                };
                match mode {
                    Mode::None => {
                        if curr == '\n' {
                            line_idx += 1;
                            line_start_idx = total_idx + 1;
                            break;
                        }
                        if char::is_alphabetic(curr) {
                            mode = Mode::Name;
                            token_location = location;
                            token_start_idx = total_idx;
                            break;
                        }
                        if char::is_numeric(curr) || curr == '-' || curr == '+' {
                            mode = Mode::Num;
                            token_location = location;
                            token_start_idx = total_idx;
                            break;
                        }
                        if curr == ':' {
                            tokens.push(Token { location, data: TokenData::Colon });
                            break;
                        }
                        if curr == ';' {
                            tokens.push(Token { location, data: TokenData::Semicolon });
                            break;
                        }
                        if curr == ',' {
                            tokens.push(Token { location, data: TokenData::Comma });
                            break;
                        }
                        if curr == '.' {
                            tokens.push(Token { location, data: TokenData::Period });
                            break;
                        }
                        if curr == '"' {
                            mode = Mode::Quot;
                            token_location = location;
                            token_start_idx = total_idx;
                            break;
                        }
                        if curr == '#' {
                            mode = Mode::Comm;
                            break;
                        }
                        if char::is_whitespace(curr) {
                            break;
                        }
                        return Err(Error::InvalidCharacter(location, curr));
                    }
                    Mode::Comm => {
                        if curr == '\n' {
                            mode = Mode::None;
                        } else {
                            break;
                        }
                    }
                    Mode::Name => {
                        if !curr.is_alphanumeric() && curr != '_' {
                            let token = &assembly[token_start_idx..total_idx];
                            tokens.push(Token {
                                location: token_location,
                                data: TokenData::Identifier(token),
                            });
                            mode = Mode::None;
                        } else {
                            break;
                        }
                    }
                    Mode::Num => {
                        let has_separator = assembly[token_start_idx..total_idx].contains('.');
                        if !(curr.is_numeric() || curr == '.' && !has_separator) {
                            let token = &assembly[token_start_idx..total_idx];
                            tokens.push(Token {
                                location: token_location,
                                data: TokenData::Number(token),
                            });
                            mode = Mode::None;
                        } else {
                            break;
                        }
                    }
                    Mode::Quot => {
                        if curr == '"' {
                            let token = &assembly[token_start_idx + 1..total_idx];
                            tokens.push(Token {
                                location: token_location,
                                data: TokenData::Quoted(token),
                            });
                            mode = Mode::None;
                            continue 'outer;
                        } else {
                            break;
                        }
                    }
                }
            }
        }

        if let Mode::Quot = mode {
            return Err(Error::UnclosedDelimiter(token_location));
        }

        /*for token in &tokens {
            println!("{}", token);
        }*/

        Ok(Self { tokens })
    }

    pub fn walk<'b>(&'b self) -> TokenWalker<'b, 'a> {
        TokenWalker {
            tokens: &self.tokens,
            current: 0,
            backtrack: Vec::new(),
        }
    }
}


struct TokenWalker<'a, 'b> {
    tokens: &'a Vec<Token<'b>>,
    current: usize,
    backtrack: Vec<usize>,
}

impl<'a, 'b> TokenWalker<'a, 'b> {
    #[inline]
    pub fn done(&self) -> bool {
        self.current >= self.tokens.len()
    }

    #[inline]
    pub fn location(&self) -> Option<SourceLocation> {
        self.tokens.get(self.current).map(|t| t.location)
    }

    #[inline]
    pub fn checkpoint(&mut self) {
        self.backtrack.push(self.current);
    }

    #[inline]
    pub fn forget(&mut self) {
        self.backtrack.pop().expect("Backtrack stack is empty!");
    }

    #[inline]
    pub fn backtrack(&mut self) {
        self.current = self.backtrack.pop().expect("Backtrack stack is empty!");
    }

    #[inline]
    pub fn expect_identifier(&mut self) -> Result<&'b str, Error> {
        match self.tokens.get(self.current) {
            Some(Token { data: TokenData::Identifier(name), .. }) => {
                self.current += 1;
                Ok(*name)
            }
            Some(Token { location, .. }) =>
                Err(Error::UnexpectedToken(*location, "Expected identifier!".to_owned())),
            None =>
                Err(Error::UnexpectedEndOfStream),
        }
    }

    #[inline]
    pub fn expect_number_literal(&mut self) -> Result<&'b str, Error> {
        match self.tokens.get(self.current) {
            Some(Token { data: TokenData::Number(value), .. }) => {
                self.current += 1;
                Ok(*value)
            }
            Some(Token { location, .. }) =>
                Err(Error::UnexpectedToken(*location, "Expected integer!".to_owned())),
            None =>
                Err(Error::UnexpectedEndOfStream),
        }
    }

    #[inline]
    pub fn expect_number<T>(&mut self) -> Result<T, Error>
        where T: std::str::FromStr,
              <T as std::str::FromStr>::Err: std::string::ToString {
        self.checkpoint();
        let literal = try_or!(self.expect_number_literal(), self.backtrack());
        let location = self.location().unwrap();
        let value = literal.parse::<T>()
            .map_err(|e| Error::InvalidInteger(location, e.to_string()));
        let value = try_or!(value, self.backtrack());
        self.forget();
        Ok(value)
    }

    #[inline]
    pub fn expect_colon(&mut self) -> Result<(), Error> {
        match self.tokens.get(self.current) {
            Some(Token { data: TokenData::Colon, .. }) => {
                self.current += 1;
                Ok(())
            }
            Some(Token { location, .. }) =>
                Err(Error::UnexpectedToken(*location, "Expected colon (:)!".to_owned())),
            None =>
                Err(Error::UnexpectedEndOfStream),
        }
    }

    #[inline]
    pub fn expect_semicolon(&mut self) -> Result<(), Error> {
        match self.tokens.get(self.current) {
            Some(Token { data: TokenData::Semicolon, .. }) => {
                self.current += 1;
                Ok(())
            }
            Some(Token { location, .. }) =>
                Err(Error::UnexpectedToken(*location, "Expected semicolon (;)!".to_owned())),
            None =>
                Err(Error::UnexpectedEndOfStream),
        }
    }

    #[inline]
    pub fn expect_comma(&mut self) -> Result<(), Error> {
        match self.tokens.get(self.current) {
            Some(Token { data: TokenData::Comma, .. }) => {
                self.current += 1;
                Ok(())
            }
            Some(Token { location, .. }) =>
                Err(Error::UnexpectedToken(*location, "Expected comma (,)!".to_owned())),
            None =>
                Err(Error::UnexpectedEndOfStream),
        }
    }

    #[inline]
    pub fn expect_period(&mut self) -> Result<(), Error> {
        match self.tokens.get(self.current) {
            Some(Token { data: TokenData::Period, .. }) => {
                self.current += 1;
                Ok(())
            }
            Some(Token { location, .. }) =>
                Err(Error::UnexpectedToken(*location, "Expected period (.)!".to_owned())),
            None =>
                Err(Error::UnexpectedEndOfStream),
        }
    }

    #[inline]
    pub fn expect_quoted(&mut self) -> Result<&'b str, Error> {
        match self.tokens.get(self.current) {
            Some(Token { data: TokenData::Quoted(string), .. }) => {
                self.current += 1;
                Ok(*string)
            }
            Some(Token { location, .. }) =>
                Err(Error::UnexpectedToken(*location, "Expected string!".to_owned())),
            None =>
                Err(Error::UnexpectedEndOfStream),
        }
    }
}


#[derive(Debug)]
struct Label<'a> {
    name: &'a str,
    location: SourceLocation,
}

impl<'a> Label<'a> {
    #[inline]
    pub fn try_parse(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        tokens.checkpoint();
        let location = tokens.location();
        let name = try_or!(tokens.expect_identifier(), tokens.backtrack());
        try_or!(tokens.expect_colon(), tokens.backtrack());
        tokens.forget();
        Ok(Label { name, location: location.unwrap() })
    }
}


#[derive(Debug)]
struct Opcode<'a> {
    name: &'a str,
    location: SourceLocation,
}

impl<'a> Opcode<'a> {
    #[inline]
    pub fn try_parse(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        let location = tokens.location();
        let opcode = tokens.expect_identifier()?;
        Ok(Self { name: opcode, location: location.unwrap() })
    }
}


#[derive(Debug)]
enum InstrArgs<'a> {
    None,
    Abc(u32),
    ABc(u8, u16),
    ABC(u8, u8, u8),
    Rel(Label<'a>),
    Abs(Label<'a>),
}

impl<'a> InstrArgs<'a> {
    #[inline]
    pub fn try_parse_abc(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        let abc = tokens.expect_number()?;
        Ok(InstrArgs::Abc(abc))
    }

    #[inline]
    pub fn try_parse_a_bc(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        tokens.checkpoint();
        let a = try_or!(tokens.expect_number(), tokens.backtrack());
        try_or!(tokens.expect_comma(), tokens.backtrack());
        let bc = try_or!(tokens.expect_number(), tokens.backtrack());
        tokens.forget();
        Ok(InstrArgs::ABc(a, bc))
    }

    #[inline]
    pub fn try_parse_bc(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        let bc = tokens.expect_number()?;
        Ok(InstrArgs::ABc(0, bc))
    }

    #[inline]
    pub fn try_parse_a_b_c(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        tokens.checkpoint();
        let a = try_or!(tokens.expect_number(), tokens.backtrack());
        try_or!(tokens.expect_comma(), tokens.backtrack());
        let b = try_or!(tokens.expect_number(), tokens.backtrack());
        try_or!(tokens.expect_comma(), tokens.backtrack());
        let c = try_or!(tokens.expect_number(), tokens.backtrack());
        tokens.forget();
        Ok(InstrArgs::ABC(a, b, c))
    }

    #[inline]
    pub fn try_parse_a_b(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        tokens.checkpoint();
        let a = try_or!(tokens.expect_number(), tokens.backtrack());
        try_or!(tokens.expect_comma(), tokens.backtrack());
        let b = try_or!(tokens.expect_number(), tokens.backtrack());
        tokens.forget();
        Ok(InstrArgs::ABC(a, b, 0))
    }

    #[inline]
    pub fn try_parse_a(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        let a = tokens.expect_number()?;
        Ok(InstrArgs::ABC(a, 0, 0))
    }

    #[inline]
    pub fn try_parse_rel(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        let location = tokens.location();
        let name = tokens.expect_identifier()?;
        let location = location.unwrap();
        Ok(InstrArgs::Rel(Label { name, location }))
    }

    #[inline]
    pub fn try_parse_abs(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        let location = tokens.location();
        let name = tokens.expect_identifier()?;
        let location = location.unwrap();
        Ok(InstrArgs::Abs(Label { name, location }))
    }
}


macro_rules! arg_parser {
    ($t:expr; $o:ident) => {{
        Ok(InstrArgs::None)
    }};
    ($t:expr; $o:ident ABC) => {{
        InstrArgs::try_parse_abc($t)
    }};
    ($t:expr; $o:ident A BC) => {{
        InstrArgs::try_parse_a_bc($t)
    }};
    ($t:expr; $o:ident BC) => {{
        InstrArgs::try_parse_bc($t)
    }};
    ($t:expr; $o:ident A B C) => {{
        InstrArgs::try_parse_a_b_c($t)
    }};
    ($t:expr; $o:ident A B) => {{
        InstrArgs::try_parse_a_b($t)
    }};
    ($t:expr; $o:ident A) => {{
        InstrArgs::try_parse_a($t)
    }};
    ($t:expr; $o:ident REL) => {{
        InstrArgs::try_parse_rel($t)
    }};
    ($t:expr; $o:ident ABS) => {{
        InstrArgs::try_parse_abs($t)
    }};
}

macro_rules! arg_parsers {
    ($t:expr, $v:expr ; $($o:ident $($a:ident)*);* $(;)?) => {
        match $v.name {
            $(stringify!($o) => arg_parser!($t; $o $($a)*),)*
            _ => Err(Error::InvalidOpcode($v.location, $v.name.to_owned())),
        }
    }
}


#[derive(Debug)]
struct Instruction<'a> {
    opcode: Opcode<'a>,
    args: InstrArgs<'a>,
}

impl<'a> Instruction<'a> {
    #[inline]
    pub fn try_parse(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        tokens.checkpoint();
        let opcode = try_or!(Opcode::try_parse(tokens), tokens.backtrack());
        let args = arg_parsers![
            tokens, opcode;

            Nop;
            Halt;

            Pshp ABS;
            Pshi ABS;
            Pshu ABS;
            Pshf ABS;
            Pshs ABS;
            Pshl;
            Pshc ABS;

            Pop;
            Top BC;
            Swap A B;
            Drop BC;
            Down BC;

            Add;
            Sub;
            Mul;
            Div;
            Mod;
            Cmp;

            Jump REL;
            Jeq REL;
            Jlg REL;
            Jsm REL;
            Jleq REL;
            Jseq REL;

            Out;

            Getl;
            Gete;

            Call;
            Ret;
            Calx;
        ];
        let args = try_or!(args, tokens.backtrack());
        try_or!(tokens.expect_semicolon(), tokens.backtrack());
        tokens.forget();
        Ok(Self { opcode, args })
    }

    pub fn encode(&self, ptr: usize, labels: &HashMap<&'a str, usize>) -> Result<u32, Error> {
        let opcode = crate::vm::Opcode::try_from_str(self.opcode.name)
            .ok_or_else(|| {
                let location = self.opcode.location;
                let name = self.opcode.name.to_owned();
                Error::InvalidOpcode(location, name)
            })?;
        let instr = match &self.args {
            InstrArgs::None => crate::vm::Instr::new_opcode(opcode),
            InstrArgs::Abc(abc) => crate::vm::Instr::new_abc(opcode, *abc),
            InstrArgs::ABc(a, bc) => crate::vm::Instr::new_a_bc(opcode, *a, *bc),
            InstrArgs::ABC(a, b, c) => crate::vm::Instr::new_a_b_c(opcode, *a, *b, *c),
            InstrArgs::Rel(label) => {
                let ptr_to = *labels.get(&label.name)
                    .ok_or(Error::UnknownLabel(label.location, label.name.to_owned()))? as isize;
                let offset = (ptr_to - ptr as isize - 1) as i16 as u16;
                crate::vm::Instr::new_a_bc(opcode, 0, offset)
            }
            InstrArgs::Abs(label) => {
                let ptr_to = *labels.get(&label.name)
                    .ok_or(Error::UnknownLabel(label.location, label.name.to_owned()))? as u16;
                crate::vm::Instr::new_a_bc(opcode, 0, ptr_to)
            }
        };
        Ok(instr.into())
    }
}


#[derive(Debug)]
enum ConstData<'a> {
    Signed(i64),
    Unsigned(u64),
    Float(f64),
    String(&'a str),
}

impl<'a> ConstData<'a> {
    #[inline]
    pub fn try_parse(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        let location = tokens.location();
        if let Ok(value) = tokens.expect_number() {
            return Ok(ConstData::Unsigned(value));
        } else if let Ok(value) = tokens.expect_number() {
            return Ok(ConstData::Signed(value));
        } else if let Ok(value) = tokens.expect_number() {
            return Ok(ConstData::Float(value));
        } else if let Ok(data) = tokens.expect_quoted() {
            return Ok(ConstData::String(data));
        }
        let location = location.unwrap();
        let msg = "Expected integer or string!".to_owned();
        Err(Error::UnexpectedToken(location, msg))
    }

    pub fn encode(&self, stream: &mut Vec<u32>) {
        match self {
            ConstData::Signed(value) => {
                let higher = (*value >> 32) as u32;
                let lower = *value as u32;
                stream.push(higher);
                stream.push(lower);
            }
            ConstData::Unsigned(value) => {
                let higher = (*value >> 32) as u32;
                let lower = *value as u32;
                stream.push(higher);
                stream.push(lower);
            }
            ConstData::Float(value) => {
                let value: u64 = unsafe { mem::transmute(*value) };
                let higher = (value >> 32) as u32;
                let lower = value as u32;
                stream.push(higher);
                stream.push(lower);
            }
            ConstData::String(value) => {
                let mut iter = value.bytes().chain("\0".bytes());
                while let Some(b0) = iter.next() {
                    let b0 = b0 as u32;
                    let b1 = iter.next().unwrap_or(0) as u32;
                    let b2 = iter.next().unwrap_or(0) as u32;
                    let b3 = iter.next().unwrap_or(0) as u32;
                    let encoded = (b0 << 24) | (b1 << 16) | (b2 << 8) | b3;
                    stream.push(encoded)
                }
            }
        }
    }
}


#[derive(Debug)]
enum DirectiveType<'a> {
    EntryPoint,
    Const(ConstData<'a>),
}


#[derive(Debug)]
struct Directive<'a> {
    location: SourceLocation,
    variant: DirectiveType<'a>,
}

impl<'a> Directive<'a> {
    pub fn try_parse(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        tokens.checkpoint();
        try_or!(tokens.expect_period(), tokens.backtrack());
        let location = tokens.location();
        let name = try_or!(tokens.expect_identifier(), tokens.backtrack());
        let variant = match name {
            "entry" => DirectiveType::EntryPoint,
            "const" => {
                let data = try_or!(ConstData::try_parse(tokens), tokens.backtrack());
                DirectiveType::Const(data)
            }
            _ => {
                tokens.backtrack();
                return Err(Error::InvalidDirective(location.unwrap(), name.to_owned()));
            }
        };
        tokens.forget();
        Ok(Self { location: location.unwrap(), variant })
    }

    pub fn emit(&self, stream: &mut Vec<u32>) {
        match &self.variant {
            DirectiveType::EntryPoint => (),
            DirectiveType::Const(data) => data.encode(stream),
        }
    }
}


#[derive(Debug)]
enum ElementType<'a> {
    Instruction(Instruction<'a>),
    Directive(Directive<'a>),
}

impl<'a> ElementType<'a> {
    #[inline]
    pub fn try_parse(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        tokens.checkpoint();
        let err_dir = match Directive::try_parse(tokens) {
            Ok(directive) => {
                tokens.forget();
                return Ok(ElementType::Directive(directive));
            }
            Err(err) => {
                tokens.backtrack();
                err
            }
        };

        tokens.checkpoint();
        let err_instr = match Instruction::try_parse(tokens) {
            Ok(instruction) => {
                tokens.forget();
                return Ok(ElementType::Instruction(instruction));
            }
            Err(err) => {
                tokens.backtrack();
                err
            }
        };

        match (err_instr, err_dir) {
            (Error::UnexpectedEndOfStream, Error::UnexpectedEndOfStream) => {
                let location = tokens.location().unwrap();
                Err(Error::UnexpectedToken(location, "Expected period or identifier!".to_owned()))
            }
            (err_instr, Error::UnexpectedEndOfStream) => Err(err_instr),
            (Error::UnexpectedEndOfStream, err_dir) => Err(err_dir),
            (err_instr, err_dir) => {
                let loc_instr = err_instr.location().unwrap();
                let loc_dir = err_dir.location().unwrap();
                if loc_instr >= loc_dir {
                    Err(err_instr)
                } else {
                    Err(err_dir)
                }
            }
        }
    }
}


#[derive(Debug)]
struct Element<'a> {
    label: Option<Label<'a>>,
    variant: ElementType<'a>,
}

impl<'a> Element<'a> {
    #[inline]
    pub fn try_parse(tokens: &mut TokenWalker<'_, 'a>) -> Result<Self, Error> {
        let label = Label::try_parse(tokens).ok();
        let variant = ElementType::try_parse(tokens)?;
        Ok(Self { label, variant })
    }

    pub fn emit(&self,
                ptr: usize,
                labels: &HashMap<&'a str, usize>,
                stream: &mut Vec<u32>)
                -> Result<(), Error> {
        match &self.variant {
            ElementType::Directive(directive) =>
                directive.emit(stream),
            ElementType::Instruction(instruction) =>
                stream.push(instruction.encode(ptr, labels)?),
        }
        Ok(())
    }
}


fn parse<'a>(tokens: &mut TokenWalker<'_, 'a>) -> Result<Vec<Element<'a>>, Error> {
    let mut elements = Vec::new();
    while !tokens.done() {
        let element = Element::try_parse(tokens)?;
        elements.push(element);
    }
    Ok(elements)
}


pub fn load(source: &str) -> Result<crate::vm::Program, Error> {
    let tokens = TokenList::tokenize(source)?;
    let elements = parse(&mut tokens.walk())?;
    let mut labels = HashMap::new();
    let mut ptr = 0 as usize;
    for element in elements.iter() {
        if let Element { label: Some(label), .. } = element {
            if let Some(_) = labels.insert(label.name, ptr) {
                return Err(Error::DuplicateLabel(label.location, label.name.to_owned()));
            }
        }
        if let Element { variant: ElementType::Directive(directive), .. } = element {
            if let DirectiveType::Const(data) = &directive.variant {
                match data {
                    ConstData::Signed(_)
                    | ConstData::Unsigned(_)
                    | ConstData::Float(_) => ptr += 2,
                    ConstData::String(string) => ptr += (string.len() + 1) / 4 + 1,
                }
            }
        } else {
            ptr += 1;
        }
    }

    let mut entry = None;
    let mut data = Vec::with_capacity(elements.len());
    for element in elements.iter() {
        if let Element {
            variant: ElementType::Directive(
                Directive { variant: DirectiveType::EntryPoint, .. }
            ), ..
        } = element {
            entry = Some(data.len());
        } else {
            element.emit(data.len(), &labels, &mut data)?;
        }
    }

    let entry = entry.ok_or(Error::MissingEntryPoint)?;

    Ok(crate::vm::Program { entry, data, labels })
}


#[cfg(test)]
mod tests {}