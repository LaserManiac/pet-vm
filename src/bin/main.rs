use std::rc::Rc;

#[macro_use]
extern crate petvm;

use petvm::vm::PetVM;

fn main() {
    let program = std::fs::read_to_string("res/program.ass")
        .expect("Could not open program file!");
    match petvm::assembler::load(&program) {
        Ok(program) => {
            println!("\n{}", program.view(program.entry, program.data.len()));

            let mut vm = PetVM::new(&program, 1024);
            vm.bind_extern("split", define_extern! {
                a: Rc<String>, b: char => Vec<String> {
                    a.split(b).map(|s| s.to_owned()).collect::<Vec<String>>()
                }
            });

            match vm.run() {
                Ok(res) => println!("Success: {}", res),
                Err(err) => {
                    let pc = vm.pc() - 1;
                    let view = vm.program().view(pc, 5);
                    let stack = vm.stack();
                    println!("[{:?}] PC: {}\n{}STACK: {}", err, pc, view, stack);
                },
            }
        }
        Err(err) => println!("Error loading .ass file: {}", err),
    }
}
