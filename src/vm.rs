use std::mem;
use std::collections::HashMap;
use std::fmt::{Display, Formatter, Result as FmtResult};

use crate::value::Value;
use crate::stack::{Stack, StackPtr};


#[derive(Debug)]
pub enum Error {
    StackUnderflow,
    StackOverflow,
    InvalidConstPtr(usize),
    InvalidStackPtr(StackPtr),
    InvalidStackOffset(StackPtr),
    InvalidString,
    InvalidChar,
    InvalidValueType,
    MissingExternFunction(String),
    GenericError(String),
}

impl<T: Into<String>> From<T> for Error {
    fn from(string: T) -> Self {
        Error::GenericError(string.into())
    }
}


macro_rules! gen_opcodes {
    ($($x:ident),* $(,)?) => {
        #[derive(Copy, Clone, Eq, PartialEq, Debug)]
        #[repr(u8)]
        pub enum Opcode {
            $($x,)*
        }

        impl Opcode {
            pub fn try_from_str(s: &str) -> Option<Self> {
                match s {
                    $(stringify!($x) => Some(Opcode::$x),)*
                    _ => None,
                }
            }
        }

        impl Display for Opcode {
            fn fmt(&self, f: &mut Formatter) -> FmtResult {
                match self {
                    $(Opcode::$x => Display::fmt(stringify!($x), f),)*
                }
            }
        }
    }
}

gen_opcodes! {
    Nop,    // Does absolutely nothing
    Halt,   // Pop value from the stack and return it from the program

    Pshp,   // Push pointer to the given label onto the stack
    Pshi,   // Push signed int at given label onto the stack
    Pshu,   // Push unsigned int at given label onto the stack
    Pshf,   // Push float at given label onto the stack
    Pshs,   // Push string at given label onto the stack
    Pshl,   // Push an empty list onto the stack
    Pshc,   // Push char at given label onto the stack

    Pop,    // Pop a value from the stack
    Top,    // Push a copy of a value in the stack at the given offset onto the stack
    Swap,   // Swap two values in the stack
    Drop,   // Pop a value from the stack and drop it into the stack to the given offset
    Down,   // Pop a given amount of values from the stack

    Add,    // Pop two values from the stack and add them, then push the result
    Sub,    // Pop two values from the stack and subtract them, then push the result
    Mul,    // Pop two values from the stack and multiply them, then push the result
    Div,    // Pop two values from the stack and divide them, then push the result
    Mod,    // Pop two values from the stack and modulo them, then push the result
    Cmp,    // Pop two values from the stack and compare them, result is put into cmp_flag

    Jump,   // Jump a given number of instructions
    Jeq,    // Jump a given number of instructions if cmp_flag is 0
    Jlg,    // Jump a given number of instructions if cmp_flag is 1
    Jsm,    // Jump a given number of instructions if cmp_flag is -1
    Jleq,   // Jump a given number of instructions if cmp_flag is 0 or 1
    Jseq,   // Jump a given number of instructions if cmp_flag is 0 or -1

    Out,    // Pop a value from the stack and print it out to the screen

    Getl,   // Pop a value from the stack and get its length
    Gete,   // Pop value and an index from the stack, then push a copy of the element at the index

    Call,   // Pops a ptr from the stack and calls that procedure, pushes a return ptr to the stack
    Ret,    // Return from a procedure, the return ptr must be on top of the stack
    Calx,   // Pops a string from the stack and calls an external function with the given name
}


#[derive(Copy, Clone, Debug)]
#[repr(packed)]
pub struct Instr {
    pub opcode: Opcode,
    pub data: [u8; 3],
}

impl Instr {
    pub fn new_opcode(opcode: Opcode) -> Self {
        let data = unsafe { mem::uninitialized() };
        Instr { opcode, data }
    }

    pub fn new_a_b_c(opcode: Opcode, a: u8, b: u8, c: u8) -> Self {
        let data = [a, b, c];
        Instr { opcode, data }
    }

    pub fn new_a_bc(opcode: Opcode, a: u8, bc: u16) -> Self {
        let b = ((bc & 0xFF00) >> 8) as u8;
        let c = ((bc & 0x00FF) >> 0) as u8;
        let data = [a, b, c];
        Instr { opcode, data }
    }

    pub fn new_abc(opcode: Opcode, abc: u32) -> Self {
        assert_eq!(abc & 0xFF000000, 0, "ABC argument must be 24 bits or less!");
        let a = ((abc & 0x00_FF_00_00) >> 16) as u8;
        let b = ((abc & 0x00_00_FF_00) >> 08) as u8;
        let c = ((abc & 0x00_00_00_FF) >> 00) as u8;
        let data = [a, b, c];
        Instr { opcode, data }
    }

    pub fn new_data(data: u32) -> Self {
        Self::from(data)
    }

    pub fn get_a_b_c(&self) -> (u8, u8, u8) {
        (self.data[0], self.data[1], self.data[2])
    }

    pub fn get_a_bc(&self) -> (u8, u16) {
        let bc = (self.data[1] as u16) << 8 | self.data[2] as u16;
        (self.data[0], bc)
    }

    pub fn get_abc(&self) -> u32 {
        (self.data[0] as u32) << 16 | (self.data[1] as u32) << 8 | self.data[2] as u32
    }
}

impl Into<u32> for Instr {
    fn into(self) -> u32 {
        self.opcode as u32
            | (self.data[0] as u32) << 8
            | (self.data[1] as u32) << 16
            | (self.data[2] as u32) << 24
    }
}

impl From<u32> for Instr {
    fn from(instr: u32) -> Self {
        Instr {
            opcode: unsafe {
                std::mem::transmute((instr & 0x00_00_00_FF) as u8)
            },
            data: [
                ((instr & 0x00_00_FF_00) >> 8) as u8,
                ((instr & 0x00_FF_00_00) >> 16) as u8,
                ((instr & 0xFF_00_00_00) >> 24) as u8,
            ],
        }
    }
}


#[derive(Debug)]
pub struct Program<'a> {
    pub entry: usize,
    pub data: Vec<u32>,
    pub labels: HashMap<&'a str, usize>,
}

impl<'a> Program<'a> {
    pub fn get(&self, ptr: usize) -> Result<u32, Error> {
        self.data.get(ptr)
            .cloned()
            .ok_or(Error::InvalidConstPtr(ptr))
    }

    pub fn view(&self, ptr: usize, border: usize) -> ProgramView<'_, 'a> {
        ProgramView { program: self, border, ptr }
    }

    pub fn full_view(&self) -> ProgramView<'_, 'a> {
        ProgramView { program: self, ptr: self.entry, border: self.data.len() }
    }
}


pub struct ProgramView<'a, 'b> {
    program: &'a Program<'b>,
    ptr: usize,
    border: usize,
}

impl<'a, 'b> Display for ProgramView<'a, 'b> {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        let idx_len = f32::log10(self.program.data.len() as f32).ceil() as usize;
        let longest = self.program.labels.iter()
            .map(|(name, _)| name.len())
            .max()
            .unwrap_or(0);
        let labels_rev = self.program.labels.iter()
            .map(|(k, v)| (*v, *k))
            .collect::<HashMap<_, _>>();

        for (ptr, value) in self.program.data.iter()
            .enumerate()
            .filter(|(idx, _)| {
                let max = usize::max(*idx, self.ptr);
                let min = usize::min(*idx, self.ptr);
                max - min <= self.border
            }) {
            let label = labels_rev.get(&ptr).unwrap_or(&"");
            let cursor = if ptr == self.ptr { '<' } else { ' ' };
            f.write_fmt(format_args!(
                "{:idx_width$} | {:>width$} | {:>032b} | {}\n",
                ptr, label, value, cursor,
                idx_width = idx_len, width = longest,
            ))?;
        }

        Ok(())
    }
}


pub trait ExternFunction {
    fn run(&self, stack: &mut Stack) -> Result<(), Error>;
}

#[macro_export]
macro_rules! define_extern {
    ($stack:ident $function:block) => {{
        struct Function;

        impl petvm::vm::ExternFunction for Function {
            fn run(&self, $stack: &mut petvm::stack::Stack) -> Result<(), petvm::vm::Error>
                $function
        }

        Function
    }};
    ($($args:ident : $types:ty),* => $ret:ty $function:block) => {
        define_extern!(stack {
            $(
                let $args: $types = {
                    let value = stack.pop()
                        .ok_or(petvm::vm::Error::StackUnderflow)?;
                    let value: Option<$types> = value.into();
                    value.ok_or(petvm::vm::Error::InvalidValueType)?
                };
            )*
            let result = $function;
            let ret: $ret = result.into();
            let value = ret.into();
            stack.push(value)
                .map(|_| ())
                .ok_or(petvm::vm::Error::StackOverflow)
        });
    };
}


pub struct PetVM<'a, 'b> {
    program: &'a Program<'b>,
    stack: Stack,
    pc: usize,
    flag_cmp: i8,
    externs: HashMap<String, Box<dyn ExternFunction>>,
}

impl<'a, 'b> PetVM<'a, 'b> {
    pub fn new(program: &'a Program<'b>, stack_size: usize) -> Self {
        Self {
            program,
            stack: Stack::new(stack_size),
            pc: program.entry,
            flag_cmp: 0,
            externs: HashMap::new(),
        }
    }

    pub fn bind_extern<N, F>(&mut self, name: N, ext: F)
        where N: Into<String>,
              F: ExternFunction + 'static {
        self.externs.insert(name.into(), Box::new(ext));
    }

    pub fn program(&self) -> &Program<'_> {
        self.program
    }

    pub fn pc(&self) -> usize {
        self.pc
    }

    pub fn stack(&self) -> &Stack {
        &self.stack
    }

    pub fn run(&mut self) -> Result<Value, Error> {
        loop {
            #[cfg(feature = "debug")] let prev_pc = self.pc;

            let instr = Instr::from(self.fetch());

            #[cfg(feature = "debug")] {
                print!("[{}] {:<4} {}", prev_pc, instr.opcode, self.stack);
                crate::util::wait_for_input("");
            }

            match instr.opcode {
                Opcode::Nop => (),
                Opcode::Halt => {
                    return self.pop();
                }
                Opcode::Pshp => {
                    let (_, bc) = instr.get_a_bc();
                    let ptr = bc as usize;
                    self.push(ptr)?;
                }
                Opcode::Pshi => {
                    let (_, bc) = instr.get_a_bc();
                    let ptr = bc as usize;
                    let higher = self.program.get(ptr)? as u64;
                    let lower = self.program.get(ptr + 1)? as u64;
                    let value = ((higher << 32) | lower) as i64;
                    self.push(value)?;
                }
                Opcode::Pshu => {
                    let (_, bc) = instr.get_a_bc();
                    let ptr = bc as usize;
                    let higher = self.program.get(ptr)? as u64;
                    let lower = self.program.get(ptr + 1)? as u64;
                    let value = (higher << 32) | lower;
                    self.push(value)?;
                }
                Opcode::Pshf => {
                    let (_, bc) = instr.get_a_bc();
                    let ptr = bc as usize;
                    let higher = self.program.get(ptr)? as u64;
                    let lower = self.program.get(ptr + 1)? as u64;
                    let value = (higher << 32) | lower;
                    let value: f64 = unsafe { mem::transmute(value) };
                    self.push(value)?;
                }
                Opcode::Pshs => {
                    let (_, bc) = instr.get_a_bc();
                    let mut ptr = bc as usize;
                    let mut bytes = Vec::new();
                    loop {
                        let next = self.program.get(ptr)?;
                        let b0 = (next >> 24) as u8;
                        let b1 = (next >> 16) as u8;
                        let b2 = (next >> 8) as u8;
                        let b3 = (next >> 0) as u8;
                        if b0 != 0 { bytes.push(b0); } else { break; }
                        if b1 != 0 { bytes.push(b1); } else { break; }
                        if b2 != 0 { bytes.push(b2); } else { break; }
                        if b3 != 0 { bytes.push(b3); } else { break; }
                        ptr += 1;
                    }
                    let string = String::from_utf8(bytes)
                        .map_err(|_| Error::InvalidString)?;
                    let value = Value::from(string);
                    self.push(value)?;
                }
                Opcode::Pshl => {
                    self.push(Value::from(Vec::<Value>::new()))?;
                }
                Opcode::Pshc => {
                    let (_, bc) = instr.get_a_bc();
                    let ptr = bc as usize;
                    let higher = self.program.get(ptr)? as u64;
                    let lower = self.program.get(ptr + 1)? as u64;
                    let value = ((higher << 32) | lower) as u32;
                    let value = std::char::from_u32(value)
                        .ok_or(Error::InvalidChar)?;
                    self.push(value)?;
                }
                Opcode::Pop => {
                    self.pop()?;
                }
                Opcode::Top => {
                    let offset = instr.get_abc() as usize;
                    let ptr = self.stack.ptr() - 1 - offset;
                    if !self.stack.top(ptr) {
                        return Err(Error::StackOverflow);
                    }
                }
                Opcode::Swap => {
                    let (a, b, _) = instr.get_a_b_c();
                    let a_ptr = self.stack.ptr() - 1 - a as StackPtr;
                    let b_ptr = self.stack.ptr() - 1 - b as StackPtr;
                    self.stack.swap(a_ptr, b_ptr);
                }
                Opcode::Drop => {
                    let (_, bc) = instr.get_a_bc();
                    let offset = bc as StackPtr;
                    let new = self.pop()?;
                    let old = self.dig_mut(offset - 1)?;
                    *old = new;
                }
                Opcode::Down => {
                    let (_, bc) = instr.get_a_bc();
                    let offset = bc as StackPtr;
                    self.down(offset)?;
                }
                Opcode::Add => {
                    let b = self.pop()?;
                    let a = self.pop()?;
                    self.push(a + b)?;
                }
                Opcode::Sub => {
                    let b = self.pop()?;
                    let a = self.pop()?;
                    self.push(a - b)?;
                }
                Opcode::Mul => {
                    let b = self.pop()?;
                    let a = self.pop()?;
                    self.push(a * b)?;
                }
                Opcode::Div => {
                    let b = self.pop()?;
                    let a = self.pop()?;
                    self.push(a / b)?;
                }
                Opcode::Mod => {
                    let b = self.pop()?;
                    let a = self.pop()?;
                    self.push(a % b)?;
                }
                Opcode::Cmp => {
                    let b = self.pop()?;
                    let a = self.pop()?;
                    self.flag_cmp = crate::util::compare(a, b);
                }
                Opcode::Jump => {
                    let (_, bc) = instr.get_a_bc();
                    let offset = bc as i16 as isize;
                    self.move_pc(offset);
                }
                Opcode::Jeq => {
                    if self.flag_cmp == 0 {
                        let (_, bc) = instr.get_a_bc();
                        let offset = bc as i16 as isize;
                        self.move_pc(offset);
                    }
                }
                Opcode::Jlg => {
                    if self.flag_cmp == 1 {
                        let (_, bc) = instr.get_a_bc();
                        let offset = bc as i16 as isize;
                        self.move_pc(offset);
                    }
                }
                Opcode::Jsm => {
                    if self.flag_cmp == -1 {
                        let (_, bc) = instr.get_a_bc();
                        let offset = bc as i16 as isize;
                        self.move_pc(offset);
                    }
                }
                Opcode::Jleq => {
                    if self.flag_cmp == 0 || self.flag_cmp == 1 {
                        let (_, bc) = instr.get_a_bc();
                        let offset = bc as i16 as isize;
                        self.move_pc(offset);
                    }
                }
                Opcode::Jseq => {
                    if self.flag_cmp == 0 || self.flag_cmp == -1 {
                        let (_, bc) = instr.get_a_bc();
                        let offset = bc as i16 as isize;
                        self.move_pc(offset);
                    }
                }
                Opcode::Out => {
                    let a = self.pop()?;
                    println!("{}", a);
                }
                Opcode::Getl => {
                    let value = match self.pop()? {
                        Value::String(value) => Value::from(value.len() as u64),
                        Value::List(value) => Value::from(value.len() as u64),
                        _ => Value::Null,
                    };
                    self.push(value)?;
                }
                Opcode::Gete => {
                    let value = if let Value::Uint(index) = self.pop()? {
                        let index = index as usize;
                        match self.pop()? {
                            Value::String(value) =>
                                value.chars()
                                    .nth(index)
                                    .map(|c| Value::from(c))
                                    .unwrap_or(Value::Null),
                            Value::List(value) =>
                                value.get(index)
                                    .cloned()
                                    .unwrap_or(Value::Null),
                            _ => Value::Null,
                        }
                    } else {
                        Value::Null
                    };
                    self.push(value)?;
                }
                Opcode::Call => {
                    if let Value::Ptr(ptr) = self.pop()? {
                        self.push(self.pc)?;
                        self.pc = ptr;
                    } else {
                        return Err(Error::InvalidValueType);
                    }
                }
                Opcode::Ret => {
                    if let Value::Ptr(ptr) = self.pop()? {
                        self.pc = ptr;
                    } else {
                        return Err(Error::InvalidValueType);
                    }
                }
                Opcode::Calx => {
                    if let Value::String(name) = self.pop()? {
                        let func = self.externs.get(name.as_ref())
                            .ok_or_else(|| {
                                let name = String::clone(name.as_ref());
                                Error::MissingExternFunction(name)
                            })?;
                        func.run(&mut self.stack)?;
                    } else {
                        return Err(Error::InvalidValueType);
                    }
                }
            }
        }
    }

    #[inline]
    fn fetch(&mut self) -> u32 {
        let fetch_idx = self.pc;
        self.pc += 1;
        self.program.data[fetch_idx]
    }

    #[inline]
    fn move_pc(&mut self, amount: isize) {
        if amount >= 0 {
            self.pc += amount as usize;
        } else {
            self.pc -= -amount as usize;
        }
    }

    #[inline]
    fn push<>(&mut self, value: impl Into<Value>) -> Result<StackPtr, Error> {
        self.stack.push(value.into())
            .ok_or(Error::StackOverflow)
    }

    #[inline]
    fn pop(&mut self) -> Result<Value, Error> {
        self.stack.pop()
            .ok_or(Error::StackUnderflow)
    }

    #[inline]
    fn down(&mut self, amount: StackPtr) -> Result<(), Error> {
        for _ in 0..amount {
            self.pop()?;
        }
        Ok(())
    }

    #[inline]
    fn get_ref(&self, ptr: StackPtr) -> Result<&Value, Error> {
        self.stack.get_ref(ptr)
            .ok_or(Error::InvalidStackPtr(ptr))
    }

    fn get_mut(&mut self, ptr: StackPtr) -> Result<&mut Value, Error> {
        self.stack.get_mut(ptr)
            .ok_or(Error::InvalidStackPtr(ptr))
    }

    #[inline]
    fn dig_ref(&self, offset: StackPtr) -> Result<&Value, Error> {
        self.stack.dig_ref(offset)
            .ok_or(Error::InvalidStackOffset(offset))
    }

    #[inline]
    fn dig_mut(&mut self, offset: StackPtr) -> Result<&mut Value, Error> {
        self.stack.dig_mut(offset)
            .ok_or(Error::InvalidStackOffset(offset))
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn instr_encode_a_b_c() {
        let instr = Instr::new_a_b_c(Opcode::Nop, 1, 2, 3);
        let (a, b, c) = instr.get_a_b_c();
        assert_eq!(a, 1);
        assert_eq!(b, 2);
        assert_eq!(c, 3);
    }

    #[test]
    fn instr_encode_a_bc() {
        let instr = Instr::new_a_bc(Opcode::Nop, 111, 333);
        let (a, bc) = instr.get_a_bc();
        assert_eq!(a, 111);
        assert_eq!(bc, 333);
    }

    #[test]
    fn instr_encode_abc() {
        let instr = Instr::new_abc(Opcode::Nop, 123456);
        let abc = instr.get_abc();
        assert_eq!(abc, 123456);
    }

    #[test]
    fn instr_to_u32() {
        let instr = Instr::new_a_b_c(Opcode::Halt, 1, 2, 3);
        let instr: u32 = instr.into();
        let instr: Instr = instr.into();
        let (a, b, c) = instr.get_a_b_c();
        assert_eq!(instr.opcode, Opcode::Halt);
        assert_eq!(a, 1);
        assert_eq!(b, 2);
        assert_eq!(c, 3);
    }
}
