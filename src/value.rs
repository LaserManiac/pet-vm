use std::rc::Rc;
use std::cmp::{PartialOrd, Ordering};
use std::ops::{Add, Sub, Mul, Div, Rem};
use std::fmt::{Display, Formatter, Result as FmtResult};


#[derive(Clone, PartialEq, Debug)]
pub enum Value {
    Null,
    Ptr(usize),
    Int(i64),
    Uint(u64),
    Float(f64),
    Bool(bool),
    Char(char),
    String(Rc<String>),
    List(Rc<Vec<Value>>),
    Frame,
}

impl From<()> for Value {
    fn from(_: ()) -> Self {
        Value::Null
    }
}

impl From<usize> for Value {
    fn from(value: usize) -> Self {
        Value::Ptr(value)
    }
}

impl From<i64> for Value {
    fn from(value: i64) -> Self {
        Value::Int(value)
    }
}

impl From<u64> for Value {
    fn from(value: u64) -> Self {
        Value::Uint(value)
    }
}

impl From<f64> for Value {
    fn from(value: f64) -> Self {
        Value::Float(value)
    }
}

impl From<bool> for Value {
    fn from(value: bool) -> Self {
        Value::Bool(value)
    }
}

impl From<char> for Value {
    fn from(value: char) -> Self {
        Value::Char(value)
    }
}

impl From<&str> for Value {
    fn from(value: &str) -> Self {
        Value::String(Rc::new(String::from(value)))
    }
}

impl From<String> for Value {
    fn from(value: String) -> Self {
        Value::String(Rc::new(value))
    }
}

impl<T: Into<Value>> From<Vec<T>> for Value {
    fn from(value: Vec<T>) -> Self {
        let list = value.into_iter()
            .map(|v| Into::<Value>::into(v))
            .collect();
        Value::List(Rc::new(list))
    }
}

impl Into<Option<()>> for Value {
    fn into(self) -> Option<()> {
        if let Value::Null = self {
            Some(())
        } else {
            None
        }
    }
}

impl Into<Option<usize>> for Value {
    fn into(self) -> Option<usize> {
        if let Value::Ptr(value) = self {
            Some(value)
        } else {
            None
        }
    }
}

impl Into<Option<i64>> for Value {
    fn into(self) -> Option<i64> {
        if let Value::Int(value) = self {
            Some(value)
        } else {
            None
        }
    }
}

impl Into<Option<u64>> for Value {
    fn into(self) -> Option<u64> {
        if let Value::Uint(value) = self {
            Some(value)
        } else {
            None
        }
    }
}

impl Into<Option<f64>> for Value {
    fn into(self) -> Option<f64> {
        if let Value::Float(value) = self {
            Some(value)
        } else {
            None
        }
    }
}

impl Into<Option<bool>> for Value {
    fn into(self) -> Option<bool> {
        if let Value::Bool(value) = self {
            Some(value)
        } else {
            None
        }
    }
}

impl Into<Option<char>> for Value {
    fn into(self) -> Option<char> {
        if let Value::Char(value) = self {
            Some(value)
        } else {
            None
        }
    }
}

impl Into<Option<String>> for Value {
    fn into(self) -> Option<String> {
        if let Value::String(value) = self {
            Some(String::clone(&value))
        } else {
            None
        }
    }
}

impl Into<Option<Rc<String>>> for Value {
    fn into(self) -> Option<Rc<String>> {
        if let Value::String(value) = self {
            Some(value)
        } else {
            None
        }
    }
}

impl Into<Option<Vec<Value>>> for Value {
    fn into(self) -> Option<Vec<Value>> {
        if let Value::List(value) = self {
            Some(Vec::clone(&value))
        } else {
            None
        }
    }
}

impl Into<Option<Rc<Vec<Value>>>> for Value {
    fn into(self) -> Option<Rc<Vec<Value>>> {
        if let Value::List(value) = self {
            Some(value)
        } else {
            None
        }
    }
}

impl Display for Value {
    fn fmt(&self, f: &mut Formatter) -> FmtResult {
        match self {
            Value::Null => f.write_str("null"),
            Value::Ptr(value) => {
                Display::fmt(&'<', f)?;
                Display::fmt(value, f)?;
                Display::fmt(&'>', f)
            }
            Value::Int(value) => {
                Display::fmt(value, f)?;
                Display::fmt(&'i', f)
            }
            Value::Uint(value) => {
                Display::fmt(value, f)?;
                Display::fmt(&'u', f)
            }
            Value::Float(value) => {
                Display::fmt(value, f)?;
                Display::fmt(&'f', f)
            }
            Value::Bool(value) => Display::fmt(value, f),
            Value::Char(value) => {
                Display::fmt(&'\'', f)?;
                Display::fmt(value, f)?;
                Display::fmt(&'\'', f)
            }
            Value::String(value) =>
                Display::fmt(&format_args!("\"{}\"", value), f),
            Value::List(value) => {
                Display::fmt(&'{', f)?;
                let mut iter = value.iter();
                if let Some(value) = iter.next() {
                    Display::fmt(value, f)?;
                    for value in iter {
                        Display::fmt(&", ", f)?;
                        Display::fmt(value, f)?;
                    }
                }
                Display::fmt(&'}', f)
            }
            Value::Frame => Display::fmt(&'|', f),
        }
    }
}

impl PartialOrd for Value {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        match (self, other) {
            (Value::Int(a), Value::Int(b)) => PartialOrd::partial_cmp(a, b),
            (Value::Uint(a), Value::Uint(b)) => PartialOrd::partial_cmp(a, b),
            (Value::Float(a), Value::Float(b)) => PartialOrd::partial_cmp(a, b),
            _ => None
        }
    }
}

impl Add<Self> for Value {
    type Output = Value;

    fn add(self, rhs: Value) -> Self::Output {
        match (self, rhs) {
            (Value::Int(a), Value::Int(b)) => Value::Int(a + b),
            (Value::Uint(a), Value::Uint(b)) => Value::Uint(a + b),
            (Value::Float(a), Value::Float(b)) => Value::Float(a + b),
            (Value::String(a), Value::String(b)) => {
                let capacity = a.len() + b.len();
                let mut new = String::with_capacity(capacity);
                new.push_str(&a);
                new.push_str(&b);
                Value::from(new)
            }
            (Value::String(a), Value::Char(b)) => {
                let mut new = String::clone(&a);
                new.push(b);
                Value::from(new)
            }
            (Value::List(a), b) => {
                let mut new = Vec::clone(&a);
                new.push(b);
                Value::from(new)
            }
            _ => Value::Null
        }
    }
}

impl Sub<Self> for Value {
    type Output = Value;

    fn sub(self, rhs: Value) -> Self::Output {
        match (self, rhs) {
            (Value::Int(a), Value::Int(b)) => Value::Int(a - b),
            (Value::Uint(a), Value::Uint(b)) => Value::Uint(a - b),
            (Value::Float(a), Value::Float(b)) => Value::Float(a - b),
            _ => Value::Null
        }
    }
}

impl Mul<Self> for Value {
    type Output = Value;

    fn mul(self, rhs: Value) -> Self::Output {
        match (self, rhs) {
            (Value::Int(a), Value::Int(b)) => Value::Int(a * b),
            (Value::Uint(a), Value::Uint(b)) => Value::Uint(a * b),
            (Value::Float(a), Value::Float(b)) => Value::Float(a * b),
            _ => Value::Null
        }
    }
}

impl Div<Self> for Value {
    type Output = Value;

    fn div(self, rhs: Value) -> Self::Output {
        match (self, rhs) {
            (Value::Int(a), Value::Int(b)) if b != 0 => Value::Int(a / b),
            (Value::Uint(a), Value::Uint(b)) if b != 0 => Value::Uint(a / b),
            (Value::Float(a), Value::Float(b)) if b != 0.0 => Value::Float(a / b),
            _ => Value::Null
        }
    }
}

impl Rem<Self> for Value {
    type Output = Value;

    fn rem(self, rhs: Value) -> Self::Output {
        match (self, rhs) {
            (Value::Int(a), Value::Int(b)) => Value::Int(a % b),
            (Value::Uint(a), Value::Uint(b)) => Value::Uint(a % b),
            (Value::Float(a), Value::Float(b)) => Value::Float(a % b),
            _ => Value::Null
        }
    }
}
