use std::io::{self, Write, Read};

pub fn wait_for_input(msg: &str) {
    let mut stdout = io::stdout();
    stdout.write(msg.as_bytes()).unwrap();
    stdout.flush().unwrap();
    io::stdin().read(&mut [0]).unwrap();
}


pub fn str_equals_case_insensitive(first: &str, second: &str) -> bool {
    first.len() == second.len() && first.chars()
        .zip(second.chars())
        .all(|(a, b)| {
            a.to_lowercase()
                .zip(b.to_lowercase())
                .all(|(a, b)| a == b)
        })
}


pub fn signum<T>(value: T) -> T
    where T: std::cmp::PartialOrd<T>,
          T: std::ops::Neg<Output=T>,
          T: num::Zero + num::One {
    let zero = T::zero();
    if value > zero {
        T::one()
    } else if value < zero {
        -T::one()
    } else {
        zero
    }
}


pub fn compare<T: std::cmp::PartialOrd<E>, E>(a: T, b: E) -> i8 {
    if a > b {
        1
    } else if a < b {
        -1
    } else {
        0
    }
}


#[macro_export]
macro_rules! try_or {
    ($x:expr, $e:stmt) => {
        match $x {
            Ok(data) => data,
            Err(err) => {
                $e;
                return Err(err);
            }
        }
    }
}
